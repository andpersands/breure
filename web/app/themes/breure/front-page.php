<?php
/*
Template Name: Front page
*/
?>
<?php if (has_post_thumbnail( $post->ID ) ): ?>
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
	<div class="top-banner" style="background-image: url('<?php echo $image[0]; ?>');">
<?php else: ?>
	<div class="top-banner">
<?php endif; ?>
		<div class="container inner">
			<div class="top-content">
				<div class="top-left-content">
					<div class="page-header">
						<h1><?php echo roots_title(); ?></h1>
					</div>
					<?php while (have_posts()) : the_post();
				        the_content();
			     	endwhile; ?>
				</div>
				<div class="top-right-content">
					<div class="afgerond-project">
						<img src="/app/themes/breure/assets/img/icn-projecten.png" class="pull-left"><h6>Afgerond Project</h6>
						<h3>A4 Steenbergen/Halsteren</h3>
						<span class="opdrachgever">Opdrachtgever: MNO Vervat</span>
						<span class="werk">Werk: Aanleg A4</span>
						<span class="localitie">Localitie: Steenbergen-Halsteren</span>
						<span class="alle-projecten">&gt; <a href="/projecten">Alle projecten</a></span>
					</div>
				</div>
			</div>
			<div class="bottom-content">
				<img src="/app/themes/breure/assets/img/btn-downarrow.png" class="aligncenter">
			</div>
		</div>
	</div>
	<div class="container home-boxes">
		<div class="row">
			<div class="col-sm-4 box werkzaamheden-box">
				<span class="box-heading"><img src="/app/themes/breure/assets/img/icn-werkzaamheden.png" class="pull-left"><h5>Werkzaamheden</h5></span>
				<?php the_field('werkzaamheden_text'); ?>
				<span class="link">&gt; <a href="/werkzaamheden"><?php the_field('werkzaamheden_link_text'); ?></a></span>
			</div>
			<div class="col-sm-4 box projecten-box">
				<span class="box-heading"><img src="/app/themes/breure/assets/img/icn-projecten.png" class="pull-left"><h5>Projecten</h5></span>
				<?php the_field('projecten_text'); ?>
				<span class="link">&gt; <a href="/projecten"><?php the_field('projecten_link_text'); ?></a></span>
			</div>
			<div class="col-sm-4 box materieel-box">
				<span class="box-heading"><img src="/app/themes/breure/assets/img/icn-materieel.png" class="pull-left"><h5>Materieel</h5></span>
				<?php the_field('materieel_text'); ?>
				<span class="link">&gt; <a href="http://www.entreeding.com/info/businesscard.rxml?id=896928" target="_blank"><?php the_field('materieel_link_text'); ?></a></span>
			</div>
		</div>
	</div>




