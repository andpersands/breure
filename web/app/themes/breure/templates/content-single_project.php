<?php while (have_posts()) : the_post(); ?>
  <div class="top">
  <?php if (has_post_thumbnail( $post->ID ) ): ?>
    <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
    <div class="top-banner" style="background-image: url('<?php echo $image[0]; ?>');"></div>
    <div class="overlay"></div>
  <?php endif; ?>
    <div class="inner">
      <div class="container">
        <div class="top-content">
          <div class="page-header">
          <?php $next_post = get_adjacent_post(); ?>
           <?php if ( !empty( $next_post ) ): ?>
              <a href="<?php echo get_permalink($next_post->ID); ?>" class="prev"><img src="/app/themes/breure/assets/img/btn-arrow-left.png"></a>
           <?php endif; ?>
            <div class="head-container"><h1><?php echo roots_title(); ?></h1></div>
            <?php $next_post = get_adjacent_post(false, '', false); ?>
           <?php if ( !empty( $next_post ) ): ?>
              <a href="<?php echo get_permalink($next_post->ID); ?>" class="next"><img src="/app/themes/breure/assets/img/btn-arrow-right.png"></a>
           <?php endif; ?>
            <hr>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <article <?php post_class(); ?>>
      <div class="entry-content">
        <?php if (get_field('over_het_project')){ ?>
        <div class="row">
          <div class="col-sm-6 title">
            <h6>Over Het Project</h6>
            <hr>
          </div>
          <div class="col-sm-6 field-content">
            <?php the_field('over_het_project'); ?>
          </div>
        </div>
        <?php } ?>
        <?php if (get_field('opdrachtgever')){ ?>
        <div class="row">
          <div class="col-sm-6 title">
            <h6>Opdrachtgever</h6>
            <hr>
          </div>
          <div class="col-sm-6 field-content">
            <?php the_field('opdrachtgever'); ?>
          </div>
        </div>
        <?php } ?>
        <?php if (get_field('werk')){ ?>
        <div class="row">
          <div class="col-sm-6 title">
            <h6>Werk</h6>
            <hr>
          </div>
          <div class="col-sm-6 field-content">
            <?php the_field('werk'); ?>
          </div>
        </div>
        <?php } ?>
        <?php if (get_field('locatie')){ ?>
        <div class="row">
          <div class="col-sm-6 title">
            <h6>Locatie</h6>
            <hr>
          </div>
          <div class="col-sm-6 field-content">
            <?php the_field('locatie'); ?>
          </div>
        </div>
        <?php } ?>
        <?php if( have_rows('image_slideshow') ):
          $count = 0;
          $active = ""; ?>
          <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
            <?php while ( have_rows('image_slideshow') ) : the_row();
              $image = get_sub_field('image');
              if( !empty($image) ):
                if ($count == 0) $active = " active";
                else $active = ""; ?>
              <div class="item<?php echo $active; ?>">
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
              </div>
              <?php endif;
            $count++;
            endwhile; ?>
            </div>
            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"><img src="/app/themes/breure/assets/img/btn-arrow-left.png"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"><img src="/app/themes/breure/assets/img/btn-arrow-right.png"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
          <?php
        endif; ?>
      </div>
    </article>
  </div>
<?php endwhile; ?>
