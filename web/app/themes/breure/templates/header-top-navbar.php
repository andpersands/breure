<header class="navbar" role="banner">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="sidebar" data-target=".navbar-mobile">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo home_url(); ?>/"><img src="/app/themes/breure/assets/img/img-logo.png" /></a>
  </div>
	<?php
	/**
	 * For sticky header at all times see: http://getbootstrap.com/components/#navbar-fixed-top
	 * For sticky header after scrolling see: http://getbootstrap.com/javascript/#affix
	 */
	?>
    <nav class="collapse navbar-collapse navbar-right" role="navigation">
      <div id="rijplaten-button-container">
        <div class="rijplaten-button">
          <a href="http://www.breurerijplaten.nl/" class="align-right" target="_blank">Rijplaten Verhuur</a>
        </div>
      </div>
      <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav'));
        endif;
      ?>
    </nav>
  </div>
</header>
