<footer class="content-info" role="contentinfo">
	<div class="top-footer">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h2 class="text-center">Kunnen wij iets voor u betekenen?</h2>
					<span class="contact-link"><a href="/contact">Neem contact op</a></span>
				</div>
			</div>
		</div>
	</div>
	<div class="bottom-footer">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<?php  if (has_nav_menu('footer-menu')) :
						wp_nav_menu( array( 'theme_location' => 'footer-menu', 'menu_class' => 'nav pull-left' ) );
					endif;?>
					<div class="copy pull-left">
						<p>&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?> Grondwerken B.V.</p>
						<p class="small">Oude Heijningseweg 3</p>
						<p class="small">4794 RJ Heijningen</p>
					</div>
					<div class="logo1 pull-left">
						<img src="/app/themes/breure/assets/img/img-logo1.jpg" class="img-responsive" />
					</div>
					<div class="logo2 pull-left">
						<img src="/app/themes/breure/assets/img/img-logo2.jpg" class="img-responsive" />
					</div>
				</div>
			</div>
		</div>
	</div>
	</footer>

<?php wp_footer(); ?>
